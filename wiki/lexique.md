# Lexique

## Queer

> Terme générique qui englobe à la fois toutes les identités de genre, intersexuations, orientations sexuelles, romantiques et relationnelles qui diffèrent de la norme cisgenre, dyadique, et hétéro, ainsi que le refus de toute forme de politique d'assimilation culturelle. À l’origine une insulte, que la communauté LGBTI+ s’est réappropriée.


## Oppressions

> Une oppression est systémique c’est à dire qu’elle est perpétuée par un groupe entier – dominant – envers un groupe entier – dominé -. Le groupe dominant bénéfice de privilèges injustes sur le groupe dominé. L’oppression se manifeste par des discriminations et violences&nbsp;:

- queerphobies&nbsp;: enbyphobie, intersexophobie, transphobie, cissexisme, biphobie, lesbophobie, homophobie, follophobie, acephobie, arophobie
- racisme incluant islamophobie, antisémitisme et colonialisme
- validisme, psychophobie, grossophobie, sérophobie
- putophobie, classisme, âgisme
- sexisme, misogynie
- etc.

Pour aller plus loin&nbsp;: [Glossaire lavieenqueer](https://lavieenqueer.wordpress.com/2018/04/22/glossaire/)
