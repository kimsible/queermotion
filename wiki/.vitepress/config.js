module.exports =  {
  title: 'Wiki QueerMotion.org',
  description: `Vous trouverez ici des compléments d'informations sur le fonctionnement de l'instance PeerTube QueerMotion.org : collectif, outils, lexique, soutien...`,
  lang: 'fr_FR',
  markdown: {
    config: (md) => {
      md.use(require('markdown-it-attrs'))
      md.use(require('markdown-it-multimd-table'))
    }
  },
  themeConfig: {
    sidebar: [
      {
        text: 'Guide',
        children: [
          { text : 'Instance', link: '/instance' },
          { text : 'PeerTube', link: '/peertube' },
          { text : 'Soutien', link: '/soutien' }
        ]
      },
      {
        text: 'Ressources',
        children: [
          { text : 'Lexique', link: '/lexique' },
          { text : 'Mentions légales', link: '/mentions-legales' },
          { text : 'Outils', link: '/outils' },
        ]
      }
    ],
    head: [
      ['link', { rel: 'icon', href: '/logo.png' }]
    ],
    logo: '/logo.png'
  }
}
