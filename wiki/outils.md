# Outils

Voici une liste d'outils **demandant peu ou pas d'apprentissage**. Ces solutions sont libres et gratuites, ou au moins propriétaire gratuit (logiciel ou service en ligne).

## Import YouTube

- **titre, description, tags**&nbsp;: https://ytubetool.com
- **sous-titres**&nbsp;: https://downsub.com

## Partage chaines PeerTube

Pour partager une chaine - exemple pour **ledoecast**&nbsp;:

- **lien classique&nbsp;**: https://queermotion.org/c/ledoecast
- **lien court&nbsp;**: https://queermotion.org/@ledoecast


Pour partager un compte - exemple pour **kimsible**&nbsp;:

- **lien classique&nbsp;**: https://queermotion.org/a/kimsible
- **lien court&nbsp;**: https://queermotion.org/@kimsible

## Promotion réseaux

#### Pinta

Composition graphique simple, prise en main très facile, interface efficace, léger.

| 📥 Téléchargement |
| --- |
| Linux - installation via les dépôts officiels ou [Flathub](https://flathub.org/apps/details/com.github.PintaProject.Pinta) |
| Windows, macOS - https://pinta-project.com |

[**socialsizes.io**](https://socialsizes.io)&nbsp;: service en ligne listant résolutions image & vidéo pour réseaux sociaux

[**flaticon.com/stickers**](https://flaticon.com/stickers)&nbsp;: bibliothèque en ligne de stickers libres

[**storyset.com**](https://storyset.com)&nbsp;: bibliothèque en ligne d'illustrations libres

## Collaboration

- **groupes privés**&nbsp;: https://delta.chat

- **gestion des mots de passe**&nbsp;: https://keepassxc.org

- **transfert de fichiers**&nbsp;: via https://drop.picasoft.net (3Go)

- **sondages**&nbsp;: via https://framadate.org

- **forge logicielle des sites web**&nbsp;: https://gitlab.com (inscription requise)

- **écriture collaborative**&nbsp;: via https://hackmd.io (connexion possible avec gitlab)

## Création audio

### Introduction

Il exite deux types de logiciels pour l'édition audio&nbsp;:

- Les éditeurs à affichage spectral à **édition destructive**
- Les stations audionumériques à **édition non-destructive**

L'édition audio **non-destructive** signifie que toutes les modifications apportées sont calculées en temps réel et que l'accès au flux audio original est toujours possible.

Cette approche est souvent conseillé pour la création audio - musicale, par opposition à l'édition **destructive** que proposent des logiciels comme **Audacity**, plutôt utilisés pour des opérations rapides comme le nettoyage d'enregistrements audio&nbsp;: suppression du bruit et du souffle, amplification, rognage etc.

::: tip VSTs
Pour l'utilisation de plugins VST (Virtual Studio Technology) vous aurez besoin d'une quantité suffisante de mémoire RAM (au moins 4Go) et d'un large stockage SSD (500Go) pour un accès rapide aux libraries.
:::

::: tip Capture audio
Pour l'enregistrement audio, il vous faudra généralement une bonne carte son stéréo dédiée interne / externe (à partir de 30 euros) avec un bon micro (~100 euros) ou simplement un enregistreur pro de type « Zoom » (à partir de 150 euros).
:::

### Éditeurs spectrals

<p></p>

#### Audiomass.co

⚠️ **Édition destructive**<br>
Enregitrement et édition simple-piste à affichage spectrale.<br>
Très bonne alternative à Audacity pour **rogner** ou **nettoyer** un enregistrement audio.<br>
Il utilise le [stockage web local et persistant](https://fr.wikipedia.org/wiki/Stockage_web_local) pour l'enregistrement des sessions.<br>
Export en PCM wav et MP3

#### Ocenaudio

⚠️ **Édition destructive**<br>
⚠️ **Code source propriétaire**<br>
Enregistrement et édition simple-piste à affichage spectrale.<br>
Très bonne alternative à Audacity pour **rogner**, créer des **fondus**, **amplifier** ou **nettoyer** un enregistrement audio.<br>
Export en PCM raw, wav, caff, AAC m4a et Opus ogg

| 📥 Téléchargement |
| --- |
| Linux, Windows, macOS - https://ocenaudio.com |


### Stations audionumériques

<p></p>

#### Ardour

⚠️ **45€ sur Windows et macOS**<br>
Enregistrement, **édition non-linéaire multi-piste** (découpage, étirements, fondus, synchronication, etc.), **mastering**.<br>
Support des plugins VST.

| 📥 Téléchargement |
| --- |
| Linux - version 5 gratuite sur les dépôts officiels |
| Linux Ubuntu-based - version 6 gratuite via les [Backports PPA Ubuntu Studio](https://help.ubuntu.com/community/UbuntuStudio/BackportsPPA) |
| Windows, macOS - 45€ sur https://ardour.org |

#### GarageBand

⚠️ **Code source propriétaire**<br>
Enregistrement, **édition non-linéaire multi-piste** (découpage, étirements, fondus, synchronication, etc.) et **mastering**.<br>
Support des plugins VST.

| 📥 Téléchargement |
| --- |
| macOS - https://apple.com/mac/garageband |


#### Cakewalk

⚠️ **Code source propriétaire**<br>
⚠️ **Inscription sur le cloud requise**<br>
Enregistrement, **édition non-linéaire multi-piste** (découpage, étirements, fondus, synchronication, etc.) et **mastering**.<br>
Support des plugins VST.

| 📥 Téléchargement |
| --- |
| Windows - https://www.bandlab.com/products/cakewalk |

#### LMMS

Séquenceur et synthétiseur équivalent de Fruity Loops Studio mais **sans enregistreur audio**.<br>
Support des plugins VST.

| 📥 Téléchargement |
| --- |
| Linux - installation via les dépôts officiels ou [Flathub](https://flathub.org/apps/details/io.lmms.LMMS)
| Windows, macOS - https://lmms.io |


## Création vidéo

<p></p>

### Enregistrement & stream vidéo

<p></p>

#### OSB Studio

Enregistrement & stream vidéo - **compatible PeerTube**.

| 📥 Téléchargement |
| --- |
| Windows, Linux, Mac - https://obsproject.com |



### Sous-titres

<p></p>

#### Subtitle Composer

Très bon éditeur de sous-titres **Windows** et **Linux** avec affichage spectrale de l'audio.

| 📥 Téléchargement |
| --- |
| Windows, Linux - https://subtitlecomposer.kde.org/ |


#### SubtitleEdit

Édition de sous-titres sur **Windows**.

| 📥 Téléchargement |
| --- |
| [Github](https://github.com/SubtitleEdit/subtitleedit/releases) |


#### Gnome Subtitles

Édition de sous-titres sur **Linux**.

| 📥 Téléchargement |
| --- |
| - https://gnomesubtitles.org |
| - installation via les dépôts officiels |


#### Jubler

Édition de sous-titres sur **macOS**.

| 📥 Téléchargement |
| --- |
| https://jubler.org |


#### Aegisub

Éditeur de sous-titres dédié à la traduction, très utilisé pour le fansub.

| 📥 Téléchargement |
| --- |
| Windows, Linux, macOS - https://aegi.vmoe.info |


### Couteau suisse FFmpeg

<p></p>

#### Introduction

Voici une liste d'actions d'édition sans ré-encodage (**sans perte**) couramment utilisées&nbsp;:

- Découpage (**clip**) et collage (**join**) de séquences
- Démultiplexage (**demux**)&nbsp;: supprimer ou extraire un flux audio d'une vidéo
- Multiplexage (**mux**)&nbsp;: associer un flux audio et une vidéo

Ces actions sont possible avec la librarie **FFmpeg**, disponible en particulier sur les systèmes d'exploitation Linux.


#### Lossless Cut

Logiciel Electron **utilisant FFmpeg** potentiel successeur de VirtualDub et Avidemux.<br>
Seule interface complète à FFmpeg libre et gratuite connue à ce jour et **compatible Windows Linux et macOS**.

::: tip Public visé
Édition **rapide** et **sans besoin de ré-encodage**.<br>
Permet le « **mux, demux, cut, clip, join, merge** » pour l'audio et la vidéo, c'est à dire **mutiplexer, démultiplexer, découper et coller des séquences** sans perte.<br>
:::

| 📥 Téléchargement |
| --- |
| Windows, macOS - [GitHub](https://github.com/mifi/lossless-cut/releases) |
| Linux - installation via [Flathub](https://flathub.org/apps/details/no.mifi.losslesscut) - gestion de l'accès aux fichiers avec [Flatseal](https://flathub.org/apps/details/com.github.tchx84.Flatseal) |


#### Video Trimmer

Logiciel Gnome GTK utilisant FFmpeg.<br>
Permet de **découper uniquement**.<br>
Interface agréable et minimaliste, destiné à la découpe simple, rapide et sans perte.<br>

| 📥 Téléchargement |
| --- |
| Linux - installation via [Flathub](https://flathub.org/apps/details/org.gnome.gitlab.YaLTeR.VideoTrimmer) - gestion de l'accès aux fichiers avec [Flatseal](https://flathub.org/apps/details/com.github.tchx84.Flatseal) |


#### Ligne de commande

démultiplexage, multiplexage, conversion

::: warning Support limité sur Windows et macOS
Attention, même si possible, l'installation de la librarie FFmpeg sur Windows et macOS n'est pas aisée.

Il est conseillé d'avoir un système d'exploitation Linux pour ces opérations.

Étant donné qu'elles sont pour la plupart sans ré-encodage, vous pouvez par exemple utiliser un [Raspberry Pi](https://www.raspberrypi.org/).
:::

| 📥 Téléchargement |
| --- |
| Linux - installation via les dépôts officiels |
| Windows, macOS - https://www.ffmpeg.org |

**Correspondances extensions audio-video**

|vidéo      |audio|
| ---       | --- |
| MP4, MOV  | M4A |
| WEBM, MKV | OGG |

**Supprimer un flux audio d'une vidéo**

```bash
$ ffmpeg -i video_avec_audio.mp4 -vcodec copy -an video_sans_audio.mp4
```

L'option `-vcodec copy` permet de copier le flux vidéo du premier fichier source.<br>
L'option `-an` permet désactiver le flux audio.

**Extraire un flux audio d'une vidéo**

```bash
$ ffmpeg -i video_avec_audio.webm -acodec copy -vn audio.ogg
```

L'option `-acodec copy` permet de copier le flux audio du fichier source.<br>
L'option `-vn` permet désactiver le flux vidéo.

**Associer audio et vidéo** - La vidéo n'a pas de flux audio

```bash
$ ffmpeg -i video_sans_audio.mp4 -i audio1.m4a -i audio2.m4a -map 0 -map 1 -map 2 -codec copy -shortest video_avec_2_audios.mp4
```

L'option `-map` correspond aux flux&nbsp;: numérotés de 0 à 2.<br>
L'option `-shortest` permet de racourcir les flux les plus long.

**Conversion audio avec ré-encodage**

```bash
$ ffmpeg -i audio.wav audio.m4a
```

### Montage non-linéaire

<p></p>

#### Introduction

Les éditeurs dit « non-linéaires », mais **avec ré-encodage**, ont des fonctionnalités d'édition avancées comme l'ajout de filtres, animations, titrages, transitions, etc.

En conséquence, ils auront besoin d'une plus grande puissance de calcul.


::: tip Configurations minimales
En règle générale, pour tous les logiciels les configurations minimales sont&nbsp;:

- 2-Core CPU | 4Go RAM - montage léger 720p sans VFX
- 4-Core CPU | 8Go RAM - montage avancée 1080p avec VFX

À noter qu'un processeur 4 threads n'est pas équivalent à processeur à 4-Core physiques.

Sur un ordinateur portable, un Intel Core i3 sera le strict minimum pour envisager du montage vidéo HD (720p).

Plus vous aurez besoin d'effets visuels (VFX) plus vous aurez besoin de mémoire RAM.

Pour le rendu de résolutions élevées 2K à 4K, avec des effets visuels complexes, une carte graphique dédiée de gamme pro ou un processeur à 8 coeurs (8-Core) physiques minimum est fortement conseillé.
:::

<p></p>

#### Kdenlive

Très bon éditeur libre non-linéaire avec une interface agréable et facile à prendre en main pour les débutant·es.

::: tip Public visé
**Montage vidéo avancé** avec VFX légers (fonds verts, motion tracking...) sur des fichiers à taille raisonnable.
:::

👍 interface agréable, transitions, **« motion tracking »**<br>
👎 plantages avec les gros fichiers et projets avec beaucoup d'effets visuels

| 📥 Téléchargement |
| --- |
| Linux - installation via les dépôts officiels |
| Windows, macOS - https://kdenlive.org |

<p></p>

#### Shotcut

Très bon éditeur libre non-linéaire pouvant gérer les résolutions 4K avec un bon support matériel (GPU - multi-threading), la communauté est également très active.

::: tip Public visé
**Montage vidéo avancé** sans fonds verts et ni motion design complexe, sur tout type de résolution et tailles de fichiers.
:::

👍 filtres, transitions créatives, **édition audio et « keyframes » avancée**<br>
👎 pas de gestion des fonds verts et du « motion tracking »


| 📥 Téléchargement |
| --- |
| Windows, macOS - https://shotcut.org |
| Linux - installation via [Flathub](https://www.flathub.org/apps/details/org.shotcut.Shotcut) - gestion de l'accès aux fichiers avec [Flatseal](https://flathub.org/apps/details/com.github.tchx84.Flatseal)|


<p></p>

#### Blender

Très apprécié pour son module VFX, son séquenceur d'édition vidéo non-linéaire, la modélisation 3D. Développé par la fondation Blender, supporte l'encodage GPU CUDA et OpenCL.

::: tip Public visé
**Montage vidéo avancé** non linéaire complet avec VFX (Composition et Motion Tracking) et 3D complexe.
:::

👍 très bon séquenceur, flexibilité, légèreté, support VFX très complet, import AfterEffects<br>
👎 prise en main difficile mais bien documenté

| 📥 Téléchargement |
| --- |
| Linux, Windows, macOS - https://blender.org |
| Linux Ubuntu-based - [Backports PPA Ubuntu Studio](https://help.ubuntu.com/community/UbuntuStudio/BackportsPPA) |

<p></p>

#### Davinci Resolve

⚠️ **Code source propriétaire**<br>
⚠️ **Enregistrement requis et très intrusif pour la version gratuite**<br>
⚠️ **Support Linux partiel voir inexistant**<br>
⚠️ **Demande un ordinateur très puissant pour fonctionner**

Très apprécié pour ses fonctions d'étalonnage des couleurs et tout en un.

::: tip Public visé
**Montage vidéo avancé** non linéaire avec étalonnage des couleurs et VFX.
:::

👍 étalonnage, interface agréable, version gratuite souvant suffisante<br>
👎 trop peu accessible sur d'autres systèmes que Windows et des configurations matérielles moyennes

| 📥 Téléchargement |
| --- |
| Windows - https://blackmagicdesign.com/products/davinciresolve |

## Création graphique

### Introduction

::: warning Alternatives Adobe
Les solutions suivantes ont parfois des problèmes de compatibilité avec l'écosystème de Adobe, propriétaire et non officiellement documenté.
:::

### GIMP

Permet la composition graphique, la peinture numérique, le pixel art, l'édition photo.<br>
Légèreté, interface épurée, versatile.<br>
Support partiel des [PSD via un plugin](https://wiki.gimp.org/wiki/PSD_support).<br>

| 📥 Téléchargement |
| --- |
| Linux - installation via les dépôts officiels |
| Windows, macOS - https://gimp.org |

### Krita

Permet la peinture numérique, la composition graphique et l'édition photo.<br>
Apprécié pour la peinture numérique - « digital painting ».<br>
Support partiel des [PSD sans plugin](https://docs.krita.org/en/general_concepts/file_formats/file_psd.html).<br>

| 📥 Téléchargement |
| --- |
| Linux - installation via les dépôts officiels |
| Windows, macOS - https://krita.org |

### Inkscape
Permet le dessin vectoriel avancé.<br>
Pas de support des AI Illustrator.<br>
Support partiel des SVG Illustrator, préférez les EPS.<br>

| 📥 Téléchargement |
| --- |
| Linux - installation via les dépôts officiels |
| Windows, macOS - https://inkscape.org |
