# PeerTube

## Aspects

**S'installe sur un serveur**

PeerTube est un logiciel qui s’installe sur un serveur. Il permet de créer un site web d’hébergement et de diffusion de vidéos, donc de faire son « YouTube maison ».

**Décentralisé et connecté à un réseau**

La différence avec YouTube, c’est qu’il n’est pas pensé pour créer une énorme plateforme centralisant les vidéos du monde entier sur une ferme de serveurs (qui coûte horriblement cher).
Au contraire, le concept de PeerTube est de créer un réseau de nombreux petits hébergeurs de vidéos, interconnectés.

**Libre**

PeerTube est construit autour de logiciels et standards libres.

**Éthique**

Peertube n'utilise pas de publicité, pas de pistage, pas d'algorithme de censure. PeerTube utilise par défault des [licences Creative Commons](https://creativecommons.org/licenses/?lang=fr-FR).

**Instance indépendante**

Appelons les serveurs qui hébergent le logiciel PeerTube **« des instances »**.
Toutes ces instances sont crées, animées, modérées et maintenues de façon indépendante par des administrateur·ices différent·es.

## Fonctionnalités

Voici une liste de fonctionnalités utiles pour les créateur·ices:

- Upload de fichiers audios
- Transfert d'une vidéo entre chaînes d'un même compte
- **Changement de propriétaire d'une vidéo**
- Définition de la date de publication originale d'une vidéo
- Bouton de soutien sur les chaînes et les vidéos pouvant **lister un Tipeee, un Utip**, etc.
- Floutage des miniatures des vidéos NSFW (Not Safe For Work)
- Téléchargement direct ou torrent des vidéos et audios
- Édition [Markdown](https://fr.wikipedia.org/wiki/Markdown) des descriptions et en plein écran.

Désactivées sur Queermotion.org&nbsp;:

- _Import de **vidéo YouTube avec ses sous-titres**, sa miniature, sa description, etc._
- _Import de **vidéo ou audio Instagram, SoundCloud, Facebook, Bandcamp**, etc._
- _Import de vidéo torrent_

::: tip FONCTIONNALITÉS DE BASE
PeerTube inclut aussi **la grande majorité des fonctionnalités essentielles dans un service comme YouTube**&nbsp;: lives, playlists, informations de base des vidéos, sous-titres, likes, compteur de vues, commentaires, partages, abonnements aux chaînes, recherche...
:::
