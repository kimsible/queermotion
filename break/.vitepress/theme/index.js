import DefaultTheme from 'vitepress/theme'
import './custom.css'

import Columns from './components/Columns.vue'
import SocialMediaLinks from './components/SocialMediaLinks.vue'
import SocialMediaLinksQM from './components/SocialMediaLinksQM.vue'

export default {
  ...DefaultTheme,
  enhanceApp({ app }) {
    // register global components
    app.component('Columns', Columns)
    app.component('SocialMediaLinks', SocialMediaLinks)
    app.component('SocialMediaLinksQM', SocialMediaLinksQM)
  }
}
