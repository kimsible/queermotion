---
title: Charte du collectif
description: Charte du collectif QueerMotion
head:
  - - meta
    - name: title
      content: Charte du collectif QueerMotion
  - - meta
    - name: description
      content: Charte du collectif QueerMotion
  - - meta
    - property: og:description
      content: Charte du collectif QueerMotion
  - - meta
    - property: twitter:description
      content: Charte du collectif QueerMotion
  - - meta
    - property: og:title
      content: Charte du collectif QueerMotion
  - - meta
    - property: twitter:title
      content: Charte du collectif QueerMotion
  - - meta
    - property: og:url
      content: https://collectif.queermotion.org/charte.html
  - - meta
    - property: twitter:url
      content: https://collectif.queermotion.org/charte.html
---

<h1>Charte</h1>

<h2>Pourquoi une charte ?</h2>

La charte est un ensemble de règles permettant le bon fonctionnement du collectif, que **nous nous engageons** en tant que membre, à **respecter**.

Notre instance PeerTube est **un outil dont nous pouvons jouir en tant que créateur·ices grâce un effort collectif**&nbsp;: maintien, modération, contributions au code source, communication sur les campagnes de financement participatif, etc.

Aussi, **renforcer notre visibilité** ensemble, en échange d'un hébergement auto-géré obtenu grâce à nos donataires, est **indispensable pour assurer la survie de l'instance et du collectif**.

Notons que [les frais du serveur dédié avec les sauvegardes ont été chiffrés à environ 800€/an](https://liberapay.com/queermotion).


::: tip RÈGLES POUR TOUS LES MEMBRES

**1**. Tous mes contenus et échanges doivent être en accord avec **la ligne politique** et **le code de conduite** du [manifeste](/manifeste.html)

_Pas de pureté militante mais on fait gaffe quand même, respects des pronoms de chaque membre, pas de comportements oppressifs, etc._

**2**. Je m'informe, **consulte régulièrement et réponds - réagis aux annonces internes du collectif me concernant** (e-mail, DeltaChat etc.)

**3**. Je **participe à la visibilité** de QueerMotion.org sur les réseaux sociaux, sur les comptes qui m'appartiennent et/ou ceux de QueerMotion

**4**. Je **participe à l'administration** de QueerMotion.org&nbsp;: modération des vidéos, demandes d'ajout de quota ou de création de chaînes, etc.

:::
