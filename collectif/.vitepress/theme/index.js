import DefaultTheme from 'vitepress/theme'
import './custom.css'

import Layout from './Layout.vue'
import Anchor from './components/Anchor.vue'
import ButtonNextSlide from './components/ButtonNextSlide.vue'
import Card from './components/Card.vue'
import CardList from './components/CardList.vue'
import Columns from './components/Columns.vue'
import Footer from './components/Footer.vue'
import HomeBox from './components/HomeBox.vue'
import Slide from './components/Slide.vue'
import SocialMediaLinks from './components/SocialMediaLinks.vue'
import SocialMediaLinksQM from './components/SocialMediaLinksQM.vue'

export default {
  ...DefaultTheme,
  Layout,
  enhanceApp({ app }) {
    // register global components
    app.component('Anchor', Anchor)
    app.component('ButtonNextSlide', ButtonNextSlide)
    app.component('Card', Card)
    app.component('CardList', CardList)
    app.component('Columns', Columns)
    app.component('Footer', Footer)
    app.component('HomeBox', HomeBox)
    app.component('Slide', Slide)
    app.component('SocialMediaLinks', SocialMediaLinks)
    app.component('SocialMediaLinksQM', SocialMediaLinksQM)
  }
}
