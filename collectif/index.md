---
home: true
navbar: false
heroText: QueerMotion
tagline: Collectif créateur de contenu audiovisuel
sidebar: false
description: Queer / LGBTQIA+ et créateur·ices proposant divers contenus audiovisuels sur une instance PeerTube.
head:
  - - meta
    - name: title
      content: Collectif QueerMotion
  - - meta
    - name: description
      content: Queer / LGBTQIA+ et créateur·ices proposant divers contenus audiovisuels sur une instance PeerTube.
  - - meta
    - property: og:description
      content: Queer / LGBTQIA+ et créateur·ices proposant divers contenus audiovisuels sur une instance PeerTube.
  - - meta
    - property: og:title
      content: Collectif QueerMotion
  - - meta
    - property: og:url
      content: https://collectif.queermotion.org
  - - meta
    - property: og:image
      content: https://collectif.queermotion.org/home-card.png
  - - meta
    - property: twitter:image
      content: https://collectif.queermotion.org/home-card.png
  - - meta
    - property: twitter:card
      content: summary_large_image
---

<Slide>

![](/logo-full.svg) {.logo-full}

<SocialMediaLinksQM/>

<HomeBox title="Qui sommes-nous ?">
  <template v-slot:image>

  ![](/home-cover.svg) {.shadow-bottom}

  </template>

  <template v-slot:text>

  Queer et créateur·ices, nous proposons divers contenus audiovisuels sur [une instance PeerTube](/manifeste.html#instance-peertube)&nbsp;: vidéos, courts-métrages, podcasts, mises en voix, musiques...

  </template>

  <template v-slot:buttons>

  [Lire notre manifeste](/manifeste.html) [Voir nos vidéos](https://queermotion.org) {.button}

  </template>
</HomeBox>

<ButtonNextSlide icon="envelop" title="Nous contacter"/>

</Slide>


<Slide anchor="nous-contacter" title="✉️ Nous contacter" blue>

<div class=content>

![](/nous-contacter-cafe.svg =300x209) {.float-right}

Prenons le temps de nous connaître autour d'un café loin de l'instantanéité des réseaux sociaux&nbsp;!

Si vous souhaitez :

- Rejoindre le collectif
- Nous proposer un partenariat
- Nous poser des questions
- Nous envoyer des messages de soutien

Vous êtes au bon endroit !

[contact@queermotion.org](mailto:contact@queermotion.org) {.button}

**Nous essayons autant que possible de répondre rapidement aux e-mails. Cependant, il est bon de rappeler que nous sommes une petite équipe, et que nous sommes entièrement bénévoles. Nous ne pouvons donc pas garantir la rapidité de nos réponses**.

</div>

<ButtonNextSlide icon="heart" title="Remerciements"/>

</Slide>


<Slide anchor="remerciements" title="💜 Remerciements" violet>

<CardList>

::: tip Campagne Okpal
Un grand merci à tous·tes les participant·es pour leurs contributions durant notre campagne de financement [Okpal 2020](https://okpal.com/queermotion) et qui a permis le lancement de l'instance PeerTube!
:::

<Columns>

- [Framasoft](https://framasoft.org)
- Pickbullmister
- Lane
- poky
- Tempest
- Oskar
- Phie
- Fabien
- Thomas D
- Fanny Chaïbi
- Aurélie
___

- Solène
- Ael Cocagne
- Benoît Dietz
- Charlie Fabre
- Pablo Protar
- Arc
- Fr2ed
- Bobille Thomas
- Willow Dumas
- Lu Ascheoug
- M-L BT

___

- Jules Bayol
- Damien Senger
- Pierre Rudloff
- Pouhiou
- zaart tzevef
- Matthieu Guilpin
- Paul
- Céline Martinazzo
- David Leclerc
- Andrea Pandolfo
- koolfy

___

![](/nous-soutenir.svg =290x313) {.shadow-bottom}

</Columns>

</CardList>

<Footer/>

</Slide>



<!--
<Slide anchor="les-queermozs" title="Les QueerMozs" violet>

<CardList>
  <Card>

  [![](/kimsible.png =75x75)](https://queermotion.org/video-channels/keymelody) {.circle}

  **Ms KIMSIBLE** - _elle_

  _Musicienne-AdminSys_

  <SocialMediaLinks
    peertube="queermotion.org/video-channels/keymelody"
    mastodon="eldritch.cafe/@kimsible"
    instagram="mskimsible"
  />

  </Card>
  <Card>

  [![](/lane.jpg =75x75)](https://queermotion.org/accounts/lane) {.circle}

  **LANE GUENARD** - _ul_

  _Scénariste-Réalisataire_

  <SocialMediaLinks
    peertube="queermotion.org/accounts/lane"
    twitter="MxLaneGuenard"
    instagram="MxLaneGuenard"
  />

  </Card>
  <Card>

  [![](/solen.jpg =75x75)](https://queermotion.org/accounts/pabs) {.circle}

  **SOLEN DP** - _iel/lui_

  _Motion-Graphics-Designer_

  <SocialMediaLinks
    peertube="queermotion.org/accounts/pabs"
    behance="solendp999"
    instagram="pabujubi"
  />

  </Card>
  <Card>

  [![](/elora.jpg =75x75)](https://queermotion.org/video-channels/elorabertrand_channel) {.circle}

  **ELORA BERTRAND** - _iel_

  _Cinéaste_

  <SocialMediaLinks
    peertube="queermotion.org/video-channels/elorabertrand_channel"
    instagram="elora.queer.films"
  />

  </Card>
  <Card>

  [![](/em.jpg =75x75)](https://queermotion.org/video-channels/embgreen_channel) {.circle}

  **EM B.GREEN** - _iel-il/lui_

  _Musicien-Comédien_

  <SocialMediaLinks
    peertube="queermotion.org/video-channels/embgreen_channel"
    instagram="em.b.green"
    twitter="EmilyNBreen1"
  />

  </Card>
  <Card>

  [![](/charlie.jpg =75x75)](https://queermotion.org/video-channels/charlie_doe_channel) {.circle}

  **CHARLIE** - _iel/lui_

  _Podcasteur_

  <SocialMediaLinks
    peertube="queermotion.org/video-channels/charlie_doe_channel"
    instagram="ledoecast"
    twitter="ledoecast"
  />

  </Card>
</CardList>

[...rejoindre les QueerMozs ?](/nous-contacter.html) {.button}

<ButtonNextSlide/>

</Slide>

<Slide anchor="les-copaines" title="Les copaines" blue>
<CardList>
  <Card>

  [![](/toungstoune.png =75x75)](https://toungstoune.co "Toungstoune") {.circle}

  Toungstoune

  </Card>
  <Card>

  [![](/lveq.png =75x75)](https://lavieenqueer.wordpress.com "LaVieEnQueer") {.circle}

  La Vie En Queer

  </Card>
  <Card>

  [![](/representrans.png =75x75)](https://www.representrans.fr "Représentrans") {.circle}

  Représentrans

  </Card>
  <Card>

  [![](/cnb.png =75x75)](https://www.nonbinaire.org "Collectif-Non-Binaire") {.circle}

  Collectif Non-Binaire

  </Card>

  ![](/home-copaines.gif =300x300)

</CardList>

[...devenir partenaire ?](/nous-contacter.html) {.button}

<Footer/>

</Slide>
-->
