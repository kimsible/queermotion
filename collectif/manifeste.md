---
title: Manifeste
description: Manifeste du collectif QueerMotion
head:
  - - meta
    - name: title
      content: Manifeste QueerMotion
  - - meta
    - name: description
      content: Manifeste du collectif QueerMotion
  - - meta
    - property: og:description
      content: Manifeste du collectif QueerMotion
  - - meta
    - property: twitter:description
      content: Manifeste du collectif QueerMotion
  - - meta
    - property: og:title
      content: Manifeste QueerMotion
  - - meta
    - property: twitter:title
      content: Manifeste QueerMotion
  - - meta
    - property: og:url
      content: https://collectif.queermotion.org/manifeste.html
  - - meta
    - property: twitter:url
      content: https://collectif.queermotion.org/manifeste.html
  - - meta
    - property: og:image
      content: https://collectif.queermotion.org/manifeste-card.png
  - - meta
    - property: twitter:image
      content: https://collectif.queermotion.org/manifeste-card.png
  - - meta
    - property: twitter:card
      content: summary_large_image
---


<h1>Manifeste</h1>

![](/logo.svg =165x165) {.align-center}

## Pourquoi un collectif ?

QueerMotion est un collectif de fait car **nous ne souhaitons devenir** ni un service d'hébergement, ni une association généraliste LGBTQI+. **Nous souhaitons nous consacrer pleinement à la réalisation de nos créations** en tant que personnes queer, tout en gardant le contrôle sur nos données et ce, dans un espace à taille humaine et respectueux de nos luttes.

## Création

La création du collectif, a été amorcée en mars 2020 par des créateur·ices membres d'associations trans et non-binaires dont le [Collectif Non-Binaire](https://www.nonbinaire.org/), basé dans le Grand-Ouest (Nantes) et en Île-de-France (Paris).

## Nos valeurs

### Notre ligne politique

Le collectif QueerMotion se veut militant, **féministe intersectionnel, et composé uniquement de créateur·rices queer**.

Bien que notre collectif soit seulement en non-mixité queer, tous·tes nos membres fondateur·rices sont **trans** et/ou **non-binaires**, plusieurs sont aussi **handis** et **racisé·es**, donnant de leur temps également sur ces sujets dans d'autres sphères militantes ; d'où l'importance pour nous de lutter contre toute forme d'[oppressions systémiques](https://wiki.queermotion.org/#oppressions).

### L'auto-gestion face à la censure

Les gros sites de diffusion vidéo tels que YouTube adoptent une politique restrictive sur leurs contenus selon les critères capitalistes des annonceurs&nbsp;:
- « shadowban » des vidéos ou des comptes LGBTQIA+
- suppression des vidéos contenant de la nudité ou classement dans le contenu adulte
- suppression des vidéos pour cause de droit d'auteur non respecté
- démonétisation systématique

Trop souvent, ces méthodes **nous divisent** et **nous invisibilisent en tant que minorité**.

Nous pensons que l'utilisation de logiciels libres s'installant sur serveur est la voie qui nous permet de créer des plateformes « safe », indépendantes, et transparentes sur internet. La création, le maintien, la modération et l'animation d'un serveur pouvant être une alternative à un service tenu par un géant du web (Google, Amazon, Facebook, Apple, Microsoft), ne peuvent se réaliser que collectivement. Par la même occasion, un tel projet nous aide à nous rassembler et à re-créer des liens. Ensemble, nous pouvons lutter contre ces politiques et algorithmes de censure.

### Notre contenu est libre et gratuit

L'utilisation de logiciels libres et « éthiques » implique que nos contenus soit publiés sous une [licence Creatice Commons](https://creativecommons.org/licenses/?lang=fr-FR), c'est-à-dire, de **libre diffusion**.

Nous pensons que la mise en commun de notre travail par la libre diffusion est importante. Beaucoup de questions ou mouvements nous concernant et apportant une analyse intersectionnelle, comme les « Disability Studies », Études des handicaps dans leurs contextes sociaux, culturels et politiques, ou encore les « Queer Studies » ou la décolonisation, sont peu explorés ou volontairement ignorés dans le monde francophone. Certains de ces termes n’ont pas de traduction évidente en français et sont surtout des sujets de recherche anglo-saxonne, ou soi-disant uniquement en lien avec l’histoire de ces pays.

Un choix qui est évidemment politique&nbsp;: visionner, apprendre, partager, copier, etc., gratuitement et librement, permet de casser les normes élitistes visant à rendre exclusives des connaissances, des cultures, à une poignée de personnes ayant les moyens d’avoir une éducation, et très souvent d’en exclure les personnes concernées.

Enfin, la gratuité ne prive pas un·e créateur·ice d'utiliser d'autres moyens de monétisation externes plus éthiques que la publicité pour le soutien de son travail. Ces licences ne sont pas incompatibles avec les plateformes de financement participatif tels que Tipeee, Ulule ou Utip.

## Pourquoi être membre du collectif ?

### Créer en équipe

Participer à la réalisation de créations communes avec les autres membres du collectif&nbsp;: motion-design, collaborations vidéos, entraide, formation, etc.

### Aider à administrer

Aider à administrer le collectif et l'instance est aussi l'une des raisons de rejoindre le collectif.

**Maintenir** une chaîne YouTube ne demande pas autant d'énergie que le maintien d'**une instance PeerTube**&nbsp;: admin serveur, financement, communication, éducation au logiciel libre, etc.

Nous avons donc besoin de bras pour faire tourner tout ça !

### Unir nos forces

**Unir nos forces est indispensable à la survie de ce projet** car QueerMotion.org ne peut pas&nbsp;:
- être le projet d'une seule personne
- être uniquement un service d'hébergement, de redondance ou d'archives de vidéos YouTube

## Devenir membre du collectif

Plusieurs conditions sont à remplir pour devenir membre&nbsp;:

✔ Être **[**queer**](https://wiki.queermotion.org/lexique.html#queer "Terme générique qui englobe à la fois toutes les identités de genre, intersexuations, orientations sexuelles, romantiques et relationnelles qui diffèrent de la norme cisgenre, dyadique, et hétéro, ainsi que le refus de toute forme de politique d'assimilation culturelle. À l’origine une insulte, que la communauté LGBTI+ s’est réappropriée.")** et avoir **au moins 16 ans**

✔ Avoir un profil complet **avatar, nom et description** sur QueerMotion.org

_Exemple de profil&nbsp;: [queermotion.org/@kimsible](https://queermotion.org/accounts/kimsible)_

✔ Créer du contenu vidéo (motion-design, vlog...) ou audio (musique, podcast...)

✔ Adhérer au présent manifeste - **avoir une démarche d'émancipation de YouTube**

✔ Lire et signer [notre charte](https://collectif.queermotion.org/charte.html)

✔ Remplir le questionnaire suivant et le retourner à **contact[at]queermotion.org**

<<< @/questions-membre.txt
