---
sidebar: false
navbar: false
title: Rermerciements
description: Rermerciements des contributeur·ices à l'instance PeerTube QueerMotion.org
head:
  - - meta
    - property: og:url
      content: https://thanks.queermotion.org
  - - meta
    - property: og:image
      content: https://thanks.queermotion.org/card.png
  - - meta
    - property: twitter:image
      content: https://thanks.queermotion.org/card.png
  - - meta
    - property: twitter:card
      content: summary_large_image
---

# QueerMotion

![](/banner.svg) {.banner}

## 🤍 Remerciements

::: tip
Merci à tous·tes les contributeur·ices depuis la cagnotte de lancement PeerTube [Okpal 2020](https://okpal.com/queermotion)&nbsp;!
:::

<Columns>

- [Framasoft](https://framasoft.org)
- Pickbullmister
- Lane
- poky
- Tempest
- Oskar
- Phie
- Fabien
- Thomas D
- Fanny Chaïbi
- Aurélie
___

- Solène
- Ael Cocagne
- Benoît Dietz
- Charlie Fabre
- Pablo Protar
- Arc
- Fr2ed
- Bobille Thomas
- Willow Dumas
- Lu Ascheoug
- M-L BT

___

- Jules Bayol
- Damien Senger
- Pierre Rudloff
- Pouhiou
- zaart tzevef
- Matthieu Guilpin
- Paul
- Céline Martinazzo
- David Leclerc
- Andrea Pandolfo
- koolfy

___

![](/nous-soutenir.svg =290x313) {.shadow-bottom}

</Columns>

<SocialMediaLinksQM/>
