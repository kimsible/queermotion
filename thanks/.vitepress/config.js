module.exports = {
  title: 'QueerMotion',
  lang: 'fr_FR',
  markdown: {
    config: md => {
      md.use(require('markdown-it-attrs'), {
        allowedAttributes: ['id', 'class']
      })

      md.use(require('markdown-it-imsize'))

      md.use(require('markdown-it-replace-link'), {
        replaceLink: (link) => {
          if (/^\/.+\.svg$/.test(link)) {
            const svg = require('fs').readFileSync(`./thanks/public${link}`, { encoding:'utf8', flag:'r' })
            return require('mini-svg-data-uri')(svg)
          }

          return link
        }
      })
    }
  },
  head: [
    ['link', { rel: 'icon', href: '/logo.png' }],
    ['meta', { name: 'og:type', content: 'website' }]
  ],
  themeConfig: {
    logo: '/logo.png'
  }
}
